package com.ndb.nettyLearn.bio.tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

public class TcpServer {
    public static void main(String[] args) {
        try {
            /*服务器监听端口*/
            ServerSocket ss=new ServerSocket(9000);
            System.out.println("server start.....");
            while (true){
                ServerTask task = new ServerTask(ss.accept());
                new Thread(task).start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
