package com.ndb.nettyLearn.bio.tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TcpServerPool {
    private static ExecutorService pool=Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    public static void main(String[] args) {
        try {
            /*服务器监听端口*/
            ServerSocket ss=new ServerSocket(9000);
            System.out.println("server start.....");
            while (true){
                //程序会在这里阻塞，等待新客户端连接
                ServerTask task = new ServerTask(ss.accept());
                pool.execute(task);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
