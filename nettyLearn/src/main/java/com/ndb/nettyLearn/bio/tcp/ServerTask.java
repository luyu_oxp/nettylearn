package com.ndb.nettyLearn.bio.tcp;

import java.io.*;
import java.net.Socket;

public class ServerTask implements Runnable{

    private Socket socket;

    public ServerTask(Socket socket){
        this.socket=socket;
    }

    @Override
    public void run() {
        try (
                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream())
        ) {
            while (true){
                String msg = inputStream.readUTF();
                System.out.println("server accept client msg:"+msg);
                String returnStr="server:"+msg;
                if(msg.contains("exit")){
                    System.out.println("server exit....");
                    break;
                }
                System.out.println("server send:"+returnStr);
                outputStream.writeUTF(returnStr);
                outputStream.flush();
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
