package com.ndb.nettyLearn.bio.tcp;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class TcpClient {
    public static void main(String[] args) throws IOException {
        Socket socket=null;
        InetSocketAddress address=new InetSocketAddress("127.0.0.1",9000);
        ObjectOutputStream outputStream=null;
        ObjectInputStream inputStream=null;
        Scanner scanner = null;
        try{
            scanner=new Scanner(System.in);
            socket=new Socket();
            socket.connect(address);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
            System.out.println("连接成功.....");
            System.out.println("请输入.......");
            while (true){
                String msg = sendMsg(scanner);
                outputStream.writeUTF(msg);
                outputStream.flush();
                if("exit".equals(msg)){
                    System.out.println("client exit");
                    break;
                }
                System.out.println("server received:"+inputStream.readUTF());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream!=null) outputStream.close();
            if (inputStream!=null) inputStream.close();
        }
    }

    public static String sendMsg(Scanner scanner){
        String msg = scanner.next();
        return msg;
    }


}
