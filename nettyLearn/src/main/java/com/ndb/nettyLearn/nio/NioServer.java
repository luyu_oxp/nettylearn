package com.ndb.nettyLearn.nio;

public class NioServer {
    private static NioServerHandler nioServerHandler;
    public static void main(String[] args) {
        start();
    }

    private static void start() {
        if(nioServerHandler!=null){
            nioServerHandler.stop();
        }
        nioServerHandler=new NioServerHandler(8888);
        new Thread(nioServerHandler,"server").start();
    }
}
