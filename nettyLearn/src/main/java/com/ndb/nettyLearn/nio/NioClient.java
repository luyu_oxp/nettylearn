package com.ndb.nettyLearn.nio;

import java.io.IOException;
import java.util.Scanner;

public class NioClient {
    private static NioClientHandler nioClientHandler;

    public static void main(String[] args) throws IOException {
        start();
        Scanner scanner = new Scanner(System.in);
        while (sendMsg(scanner.next()));
    }

    private static boolean sendMsg(String msg) throws IOException {
        nioClientHandler.sendMsg(msg);
        return true;
    }

    private static void start() {
        if(nioClientHandler!=null){
            nioClientHandler.stop();
        }
        nioClientHandler=new NioClientHandler("127.0.0.1",8888);
        new Thread(nioClientHandler,"client").start();
    }
}
