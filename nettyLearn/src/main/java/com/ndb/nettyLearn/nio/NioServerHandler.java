package com.ndb.nettyLearn.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Set;

public class NioServerHandler implements Runnable{
    private Selector selector;
    private ServerSocketChannel serverSocketChannel;
    private volatile boolean started;

    public NioServerHandler(int port) {
        try {
            this.selector=Selector.open();
            this.serverSocketChannel=ServerSocketChannel.open();
            //非阻塞模式
            this.serverSocketChannel.configureBlocking(false);
            this.serverSocketChannel.socket().bind(new InetSocketAddress(port));
            this.serverSocketChannel.register(this.selector, SelectionKey.OP_ACCEPT);
            this.started=true;
            System.out.println("服务器已启动，端口："+port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        this.started=false;
    }

    @Override
    public void run() {
        while (started){
            try {
                this.selector.select();
                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = keys.iterator();
                SelectionKey key=null;
                while (iterator.hasNext()){
                    key = iterator.next();
                    iterator.remove();
                    try {
                        handlerInput(key);
                    } catch (IOException e) {
                        e.printStackTrace();
                        if(key!=null){
                            key.cancel();
                            if(key.channel()!=null){
                                key.channel().close();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //选择器关闭后会自动释放里面管理的资源
        if(this.selector!=null){
            try {
                this.selector.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void handlerInput(SelectionKey key) throws IOException{
        if(key.isValid()){
            //关注接收连接事件，处理新连接的请求消息
            if(key.isAcceptable()){
                ServerSocketChannel ssc=(ServerSocketChannel)key.channel();
                SocketChannel sc = ssc.accept();
                System.out.println("建立连接");
                sc.configureBlocking(false);
                sc.register(selector,SelectionKey.OP_READ);
            }

            //处理读事件，读取数据
            if(key.isReadable()){
                SocketChannel sc= (SocketChannel) key.channel();
                ByteBuffer buffer = ByteBuffer.allocate(1024);
                int count = sc.read(buffer);
                if(count>0){
                    //切换读模式
                    buffer.flip();
                    byte[] bytes=new byte[buffer.remaining()];
                    buffer.get(bytes);
                    String result = new String(bytes, "UTF-8");
                    System.out.println("客户端接收到消息："+result);
                    doWriter(sc,result);

                }else{
                    //表示当前链路已经关闭，完成四次挥手
                    key.cancel();
                    sc.close();
                }
            }
        }
    }

    private void doWriter(SocketChannel socketChannel, String msg) throws IOException {
        byte[] bytes = msg.getBytes(StandardCharsets.UTF_8);
        ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
        buffer.put(bytes);
        buffer.flip();
        socketChannel.write(buffer);

    }
}
