package com.ndb.nettyLearn.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Set;

public class NioClientHandler implements Runnable{

    private String host;
    private int port;
    private volatile boolean start;
    private Selector selector;
    private SocketChannel socketChannel;

    public NioClientHandler(String host,int port){
        this.host=host;
        this.port=port;
        try {
            this.selector=Selector.open();
            socketChannel=SocketChannel.open();
            socketChannel.configureBlocking(false);
            start=true;
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public void stop(){
        this.start=false;
    }

    @Override
    public void run() {
        //连接服务器
        try {
            doConnect();
        } catch (IOException e) {
            e.printStackTrace();
        }

        while(this.start){
            try {
                //如果选择器的事件没有发生，则会阻塞
                this.selector.select();
                //获取选择器可用的事件
                Set<SelectionKey> keys = this.selector.selectedKeys();
                Iterator<SelectionKey> iterator = keys.iterator();
                SelectionKey key=null;
                while (iterator.hasNext()){
                    key=iterator.next();
                    /**将处理过的事件移除，如果没有移除，事件会处于激活状态
                    这会导致再次处理该事件*/
                    iterator.remove();
                    try{
                        handlerInput(key);
                    }catch (Exception e){
                        if(key!=null){
                            key.cancel();
                            //TODO 关闭channel
                            if(key.channel()!=null){
                                key.channel().close();
                            }
                        }
                    }
                }
            }catch (IOException e){
                e.printStackTrace();
                System.exit(-1);
            }
        }
        //选择器关闭后会自动释放里面管理的资源
        if(this.selector!=null){
            try {
                this.selector.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 选择器注册事件处理
     * @param key
     * @throws IOException
     */
    private void handlerInput(SelectionKey key) throws IOException {
        //判断事件是否是可用的
        if(key.isValid()){
            SocketChannel sc= (SocketChannel) key.channel();
            //处理连接事件,连接未完成，处理连接相关事件
            if(key.isConnectable()){
                /**
                 * 确认通道连接已建立，方便后续IO操作
                 */
                if(sc.finishConnect()){
                    //多次调用，会覆盖前面注册事件
                    this.socketChannel.register(this.selector, SelectionKey.OP_READ);
                }else{
                    //连接失败，退出
                    System.exit(-1);
                }
            }
            //处理读事件，读取数据
            if(key.isReadable()){
                ByteBuffer buffer = ByteBuffer.allocate(1024);
                int count = sc.read(buffer);
                if(count>0){
                    //切换读模式
                    buffer.flip();
                    byte[] bytes=new byte[buffer.remaining()];
                    buffer.get(bytes);
                    String result = new String(bytes, "UTF-8");
                    System.out.println("客户端接收到消息："+result);

                }else{
                    //表示当前链路已经关闭，完成四次挥手
                    key.cancel();
                    sc.close();
                }
            }
        }
    }

    /**
     *
     */
    private void doConnect() throws IOException {
        /**
         * 如果此通道处于非阻塞模式，则调用此方法将启动非阻塞连接操作
         * 如果马上建立成功，则返回true
         * 否则返回false
         */
        if(this.socketChannel.connect(new InetSocketAddress(this.host,this.port))){
            this.socketChannel.register(this.selector,SelectionKey.OP_READ);
        }else{
            //正在连接中，进行三次握手
            //注册连接事件监听器
            this.socketChannel.register(this.selector, SelectionKey.OP_CONNECT);
        }
    }

    public void sendMsg(String msg) throws IOException {
        doWriter(this.socketChannel,msg);
    }

    private void doWriter(SocketChannel socketChannel, String msg) throws IOException {
        byte[] bytes = msg.getBytes(StandardCharsets.UTF_8);
        ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
        buffer.put(bytes);
        buffer.flip();
        socketChannel.write(buffer);

    }
}
