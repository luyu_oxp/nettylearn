package com.ndb.nettyLearn.netty.demo;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

public class ClientHandler extends SimpleChannelInboundHandler<ByteBuf> {
    /**客户端读取到数据的时候执行*/
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        System.out.println("client accept:"+msg.toString(CharsetUtil.UTF_8));
    }

    /**连接建立后*/
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("hello netty");
        ctx.writeAndFlush(Unpooled.copiedBuffer("hello netty",CharsetUtil.UTF_8));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }
}
