package com.ndb.nettyLearn.netty.demo;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;

public class NettyClient {
    private int port;
    private String host;

    public NettyClient(int port, String host) {
        this.port = port;
        this.host = host;
    }

    public void start() throws InterruptedException {
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            /**客户端启动必备*/
            Bootstrap boot = new Bootstrap();
            boot.group(group).channel(NioSocketChannel.class)
                    .remoteAddress(new InetSocketAddress(host, port))
                    .handler(new ClientHandler());
            /**连接到远程节点，阻塞直到连接完成*/
            ChannelFuture future = boot.connect().sync();
            /**阻塞,直到channel发生了关闭*/
            future.channel().closeFuture().sync();
        }finally {
            /**优雅关闭*/
            group.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        NettyClient client = new NettyClient(9999, "127.0.0.1");
        client.start();
    }
}
