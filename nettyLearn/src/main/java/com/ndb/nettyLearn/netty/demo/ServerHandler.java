package com.ndb.nettyLearn.netty.demo;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;

/**
 * 处理业务事件,找注解表示当前类是共享的类，可以同一个实例多个客户端不断连接
 * channelRead，channelReadComplete方法辨析：
 * 由于滑动窗口存在，当传输对象大小超过缓冲区大小的时候，一个完整的对象系列化后就需要传输两次
 * 此时缓冲区满了之后会调用channelReadComplete()，接收缓冲区对象
 * 而channelRead()方法则是在对象内容完全接收后才会触发调用
 */
@ChannelHandler.Sharable
public class ServerHandler extends ChannelInboundHandlerAdapter {

    /**一个完整的对象接收完成后触发*/
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf in=(ByteBuf)msg;
        System.out.println("server accept"+in.toString(CharsetUtil.UTF_8));
        ctx.write(in);
    }

    /**传输内容达到缓冲区大小的时候触发*/
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        //请求相应后立马关闭客户端连接
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }

}
